package server

import (
	"context"
	"encoding/json"
	"fmt"
	"terraform-provider-pterodactyl/provider/resources"
	"terraform-provider-pterodactyl/provider/structs"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/vicanso/go-axios"
)

func serverResourceRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	var diags diag.Diagnostics
	// always run
	providerCtx := m.(*structs.ProviderContext)
	var applicationApiKey = providerCtx.ApiKeys.AppKey
	res, err := axios.Request(&axios.Config{
		URL:     providerCtx.Url + "/api/application/servers/" + d.Id(),
		Method:  "GET",
		Headers: resources.GetHeaders(applicationApiKey),
	})
	if err != nil {
		return diag.FromErr(err)
	}
	var responseJson map[string]interface{}
	err = json.Unmarshal(res.Data, &responseJson)
	if err != nil {
		return diag.FromErr(err)
	}
	if res.Status != 200 {
		return diag.FromErr(fmt.Errorf("API returned status code %d with response: %s", res.Status, responseJson["errors"].([]interface{})[0].(map[string]interface{})["detail"].(string)))
	}
	responseJson = responseJson["attributes"].(map[string]interface{})

	d.Set("external_id", responseJson["external_id"])
	d.Set("uuid", responseJson["uuid"])
	d.Set("identifier", responseJson["identifier"])
	d.Set("name", responseJson["name"])
	d.Set("description", responseJson["description"])
	d.Set("suspended", responseJson["suspended"])
	d.Set("limits.memory", responseJson["limits"].(map[string]interface{})["memory"])
	d.Set("limits.swap", responseJson["limits"].(map[string]interface{})["swap"])
	d.Set("limits.disk", responseJson["limits"].(map[string]interface{})["disk"])
	d.Set("limits.io", responseJson["limits"].(map[string]interface{})["io"])
	d.Set("limits.cpu", responseJson["limits"].(map[string]interface{})["cpu"])
	d.Set("limits.threads", responseJson["limits"].(map[string]interface{})["threads"])
	d.Set("feature_limits.databases", responseJson["feature_limits"].(map[string]interface{})["databases"])
	d.Set("feature_limits.allocations", responseJson["feature_limits"].(map[string]interface{})["allocations"])
	d.Set("feature_limits.backups", responseJson["feature_limits"].(map[string]interface{})["backups"])
	d.Set("user", responseJson["user"])
	d.Set("node", responseJson["node"])
	d.Set("allocation", responseJson["allocation"])
	d.Set("nest", responseJson["nest"])
	d.Set("egg_id", responseJson["egg"])
	d.Set("pack", responseJson["pack"])
	d.Set("startup_command", responseJson["container"].(map[string]interface{})["startup"])
	d.Set("docker_image", responseJson["container"].(map[string]interface{})["image"])
	return diags
}
