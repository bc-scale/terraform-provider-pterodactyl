package server

import "github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"

func ResourceServer() *schema.Resource {
	return &schema.Resource{
		ReadContext:   serverResourceRead,
		CreateContext: createServer,
		DeleteContext: deleteServer,
		UpdateContext: updateServer,
		Schema: map[string]*schema.Schema{
			"start_on_install": {

				Type:     schema.TypeBool,
				Optional: true,
				Default:  true,
			},
			"name": {

				Type:     schema.TypeString,
				Required: true,
			},
			"description": {

				Type:     schema.TypeString,
				Optional: true,
				Default:  "",
			},
			"egg_id": {

				Type:     schema.TypeInt,
				Required: true,
			},
			"docker_image": {

				Type:     schema.TypeString,
				Required: true,
			},
			"startup_command": {

				Type:     schema.TypeString,
				Required: true,
			},
			"environment_variables": {

				Type:     schema.TypeMap,
				Required: true,
			},

			"limits": {

				Type: schema.TypeMap,
				// Type:     schema.TypeList,
				Required: true,
				// Elem: schema.Resource{
				// 	Schema: map[string]*schema.Schema{
				// 		"memory": {
				// 			Type:     schema.TypeInt,
				// 			Required: true,
				// 		},
				// 		"swap": {
				// 			Type:     schema.TypeInt,
				// 			Required: true,
				// 		},
				// 		"disk": {
				// 			Type:     schema.TypeInt,
				// 			Required: true,
				// 		},
				// 		"io": {
				// 			Type:     schema.TypeInt,
				// 			Required: true,
				// 		},
				// 		"cpu": {
				// 			Type:     schema.TypeInt,
				// 			Required: true,
				// 		},
				// 	},
				// },
			},
			"feature_limits": {

				Type: schema.TypeMap,
				// Type:     schema.TypeList,
				Required: true,
				// MaxItems: 1,
				// Elem: schema.Resource{
				// 	Schema: map[string]*schema.Schema{
				// 		"databases": {
				// 			Type:     schema.TypeInt,
				// 			Required: true,
				// 		},
				// 		"backups": {
				// 			Type:     schema.TypeInt,
				// 			Required: true,
				// 		},
				// 	},
				// },
			},
			"allocation_id": {

				Type:     schema.TypeInt,
				Required: true,
			},

			"user_id": {

				Type:     schema.TypeInt,
				Computed: true,
			},
			"external_id": {

				Type:     schema.TypeString,
				Optional: true,
			},
			"uuid": {

				Type:     schema.TypeString,
				Computed: true,
			},
			"identifier": {

				Type:     schema.TypeString,
				Computed: true,
			},
			"suspended": {

				Type:     schema.TypeBool,
				Computed: true,
			},
		},
	}
}
