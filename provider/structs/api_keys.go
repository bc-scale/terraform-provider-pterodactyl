package structs

type ApiKeys struct {
	ClientKey string
	AppKey    string
}
