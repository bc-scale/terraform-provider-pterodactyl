package structs

type ProviderContext struct {
	ApiKeys ApiKeys
	Url     string
}
