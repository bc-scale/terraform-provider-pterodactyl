terraform {
  required_providers {
    pterodactyl = {
      version = "0.1"
      source  = "gitlab.com/patzmc/pterodactyl"
    }
  }
}

provider "pterodactyl" {
  url = "https://panel.patzmc.com"
}
resource "pterodactyl_server" "server" {
  name = "Server with terraform!"
  # description = "My provider is working!"
  egg_id = 1
  docker_image = "ghcr.io/pterodactyl/yolks:java_8"
  startup_command = "java -jar server.jar"
  environment_variables = {
    BUNGEE_VERSION = "latest"
    SERVER_JARFILE = "server.jar"
  }
  limits = {
    memory = 512
    swap = 0
    disk = 0
    io = 500
    cpu = 0
  }
  feature_limits = {
    databases = 0
    backups = 0
  }
  allocation_id = 225
}
